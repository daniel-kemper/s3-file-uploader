import React, { useState } from 'react';
import logo from './s3-64.svg';
import './App.css';
import FileUploader from './components/FileUploader';

function App() {
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p className="App-title">
          Signed S3 Upload Drop Surface Demo
        </p>
      </header>
      <FileUploader agencyId={'a05d744d-079a-4a4e-8798-e512d2f989f1'}
                    agencyName={'Groupware Lotus'}
                    clientId={'2947159'}
                    granteeId={'a05d744d-079a-4a4e-8798-e512d2f989f1'}
                    jwt={'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Im5hbWVfaWQiOiJhdXRoMHw2MTQzNWJjY2E5ZWI2MDAwNzFmYmJmZTEiLCJzZXNzaW9uX2luZGV4IjoiX0xiYXVuX0ZCTnlXVjRycE53SGdlS0Z5Z1d0cWhDeTg3IiwiZW1haWwiOiJkYW5pZWwua2VtcGVyQGdyb3VwdGVjaC5jb20iLCJuYW1lIjoiZGFuaWVsLmtlbXBlckBncm91cHRlY2guY29tIiwidXBuIjoiZGFuaWVsLmtlbXBlckBncm91cHRlY2guY29tIiwiYXR0cmlidXRlcyI6eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6WyJhdXRoMHw2MTQzNWJjY2E5ZWI2MDAwNzFmYmJmZTEiXSwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvZW1haWxhZGRyZXNzIjpbImRhbmllbC5rZW1wZXJAZ3JvdXB0ZWNoLmNvbSJdLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjpbImRhbmllbC5rZW1wZXJAZ3JvdXB0ZWNoLmNvbSJdLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy91cG4iOlsiZGFuaWVsLmtlbXBlckBncm91cHRlY2guY29tIl0sImh0dHA6Ly9zY2hlbWFzLmF1dGgwLmNvbS9pZGVudGl0aWVzL2RlZmF1bHQvcHJvdmlkZXIiOlsiYXV0aDAiXSwiaHR0cDovL3NjaGVtYXMuYXV0aDAuY29tL2lkZW50aXRpZXMvZGVmYXVsdC9jb25uZWN0aW9uIjpbIlVzZXJuYW1lLVBhc3N3b3JkLUF1dGhlbnRpY2F0aW9uIl0sImh0dHA6Ly9zY2hlbWFzLmF1dGgwLmNvbS9pZGVudGl0aWVzL2RlZmF1bHQvaXNTb2NpYWwiOlsiZmFsc2UiXSwiaHR0cDovL3NjaGVtYXMuYXV0aDAuY29tL2NsaWVudElEIjpbInhVMExtbGV2QUNFNUhFeEtIcDJWT3NFUXFpU0R5SXo2Il0sImh0dHA6Ly9zY2hlbWFzLmF1dGgwLmNvbS9jcmVhdGVkX2F0IjpbIlRodSBTZXAgMTYgMjAyMSAxNDo1OToyNCBHTVQrMDAwMCAoQ29vcmRpbmF0ZWQgVW5pdmVyc2FsIFRpbWUpIl0sImh0dHA6Ly9zY2hlbWFzLmF1dGgwLmNvbS9lbWFpbF92ZXJpZmllZCI6WyJmYWxzZSJdLCJodHRwOi8vc2NoZW1hcy5hdXRoMC5jb20vbmlja25hbWUiOlsiZGFuaWVsLmtlbXBlciJdLCJodHRwOi8vc2NoZW1hcy5hdXRoMC5jb20vcGljdHVyZSI6WyJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8yNjE0NDgxZDZmNmNmNjVmMmNjN2RiOWMxMzRjNmY5OT9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRmRhLnBuZyJdLCJodHRwOi8vc2NoZW1hcy5hdXRoMC5jb20vdXBkYXRlZF9hdCI6WyJUaHUgU2VwIDE2IDIwMjEgMTQ6NTk6NDAgR01UKzAwMDAgKENvb3JkaW5hdGVkIFVuaXZlcnNhbCBUaW1lKSJdfSwiaWQiOiJmNjJkM2QwZC1iMGQyLTQ5M2QtOTNlNS1hYzZkNzRlZmM3MDkiLCJpc0FkbWluIjpmYWxzZSwiaXNBZ2VuY3lHcmFudGVlIjpmYWxzZSwiaXNTdWJBZ2VuY3lHcmFudGVlIjpmYWxzZX0sImlhdCI6MTYzMTgwOTAyMiwiZXhwIjoxNjQ2MjA5MDIyfQ.YY6lMhecWYiJBZYS3F7nwf75DHG6a6bEi_-M3uqZVgU'}
                    signingUrl={'https://dev.providegtech.com/api/upload/signed'}
                    deleteUrlBase={'https://dev.providegtech.com/api/upload/'}
      />
    </div>
  );
}

export default App;
