import React, { useState } from 'react';
import { LinearProgress, Snackbar } from '@material-ui/core';
import { AttachFile, Description, Image, PictureAsPdf, Theaters } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { DropzoneAreaBase } from 'material-ui-dropzone';
import axios from 'axios';

import S3Upload from './s3Upload.js'
import './FileUploader.css';

function FileUploader(props) {
  const [errorNotification, setErrorNotification] = useState(false);
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [progress, setProgress] = useState(0);
  const [loading, setLoading] = useState(false);
  const [clockToggle, setClockToggle] = useState(false);
  const queryParams = { 
    agencyId: props.agencyId,
    granteeId: props.granteeId, 
    bucketKey: `consent/${props.clientId}`, 
    metaName1: 'agency', 
    metaValue1: props.agencyName 
  };

  /* Styles */
  const linearProgressStyle = {
    width: '80%',
    marginBottom: '1em',
    visibility: loading ? 'visible' : 'hidden'
  };

  /* HOCs and supporting components */
  const StyledLinearProgress = withStyles({
    colorPrimary: {
      backgroundColor: 'rgb(170, 215, 250)'
    },
    barColorPrimary: {
      backgroundColor: '#2196f3'
    }
  })(LinearProgress);

  const preview = (evt, file) => {
    const objectUrl = URL.createObjectURL(file);
    window.open(objectUrl, '_blank');
    evt.stopPropagation();
  };

  const handlePreviewIcon = (fileObject, classes) => {
    const {type} = fileObject.file;
    const iconProps = {
      className : classes.image
    };
  
    if (type.startsWith("video/")) return <Theaters {...iconProps} />
  
    switch (type) {
      case "application/msword":
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
        return <div><Description {...iconProps} onClick={ (evt) => preview(evt, fileObject.file) }/><div className={'preview-item-text'}>{fileObject.file.name}</div></div>
      case "image/jpeg":
      case "image/jpg":
      case "image/png":
      case "image/gif":
        return <div><Image {...iconProps} onClick={ (evt) => preview(evt, fileObject.file) }/><div className={'preview-item-text'}>{fileObject.file.name}</div></div>
      case "application/pdf":
        return <div><PictureAsPdf {...iconProps}onClick={ (evt) => preview(evt, fileObject.file) }/><div className={'preview-item-text'}>{fileObject.file.name}</div></div>
      default:
        return <div><AttachFile {...iconProps} onClick={ (evt) => preview(evt, fileObject.file) }/><div className={'preview-item-text'}>{fileObject.file.name}</div></div>
    }
  };

  return (
    <div className="Upload-container">
      <StyledLinearProgress variant="determinate" value={progress} style={linearProgressStyle}/>
      <div className="Dropzone-root">
        <DropzoneAreaBase
          dropzoneText={<label>Drag & drop or click to <a href="javascript: void(0);">browse</a></label>}
          fileObjects={selectedFiles}
          onAdd={newFileObjects => {
            let files = newFileObjects.map(nfo => nfo.file);

            setErrorNotification(false);
            setSelectedFiles([].concat(selectedFiles, newFileObjects));

            const s3UploaderProps = {
              autoUpload: true,
              signingUrl: props.signingUrl,
              signingUrlMethod: 'GET',
              signingUrlHeaders: { Authorization: props.jwt },
              signingUrlQueryParams: queryParams,
              uploadRequestHeaders: { },
              onFinishS3Put: (signResult, file) => {
                setLoading(false);
                setProgress(0);
                setClockToggle(false);
              },
              onProgress: (evt) => {
                if (!clockToggle) {
                  setClockToggle(true);
                }
            
                setLoading(true);
                setProgress(evt);
              },
              onError: (evt) => {
                setClockToggle(false);
                setProgress(0);
                setErrorNotification(true);
              }
            };
            
            new S3Upload({ ...s3UploaderProps, files });
          }}
          onDelete={async (deletedFileObj) => {
            const filteredFiles = selectedFiles.filter(f => (f.file.name !== deletedFileObj.file.name));
            setSelectedFiles(filteredFiles);

            let request = axios.create({
              baseURL: props.deleteUrlBase,
              timeout: 10000,
              headers: { 'Authorization': props.jwt }
            });

            const response = await request.delete(`deleteobject?agencyId=${queryParams.agencyId}&granteeId=${queryParams.granteeId}&bucketKey=${queryParams.bucketKey}&objectName=${deletedFileObj.file.name}`);
          }}
          getPreviewIcon={handlePreviewIcon}
          previewGridClasses={ { item: 'Dropzone-items' } }
          previewGridProps={ { item: { md: 2 }, spacing: 0 } }
          showAlerts={false}
          showFileNamesInPreview={true}
        />
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={errorNotification}
        autoHideDuration={6000}
        message="A problem was encountered during the upload operation, please try again or contact Groupware support." 
        severity="error" />
    </div>
  );
}

export default FileUploader;
